---
title: Passage à BÉPO
date: 2018-08-17
tags: ["bépo"]
summary: Test
---

Cela faisait plusieurs années que j’avais entendus parler de [BÉPO](http://bepo.fr/wiki/Accueil) et que j’avais été séduis par l’idée de passer à une disposition ergonomique des touches de mon clavier.

Cependant, il a fallut trois tentatives pour y passer réellement et profiter enfin d’une frappe à l’aveugle.

## La découverte de BÉPO

Je ne saurais plus dire quand j’ai entendus parler de la disposition BÉPO, mais ça fait au minimum 7 ans.

Jusque là (et jusqu’à cette année en fait) j’ai toujours tapé en semi-aveugle sur clavier AZERTY (je veux dire par là que je regardais quasiment en continue mon clavier, même si me doigts se déplaçaient essentiellement de manière automatique).

Ma vitesse était plutôt bonne (une cinquantaine de mots minute) mais j’y trouvais tout de même un inconfort et plus ou moins une forme de « honte » de travailler sur ordinateur toute la journée sans être capable de quitter mon clavier des yeux.
Clavier BÉPO (version simplifiée)

![](/images/carte-bepo-simple.png)

## Première tentative

Ma première tentative pour passer à BÉPO s’est rapidement soldée par un échec.

Je pense savoir la raison principale de celui-ci : j’avais acheté des autocollants à mettre sur les touches. Au final, et malgré une tentative d’utilisation de [Klavaro](http://klavaro.sourceforge.net/fr/index.html) (un logiciel d’apprentissage à la dactylographie), j’ai continué de regarder le clavier et je n’ai pas persévéré comme le changement de layout peut le nécessiter.

## Deuxième tentative

Quelques années plus tard, j’ai refait une tentative (dont je ne me souviens pas vraiment).

Ça a duré encore moins longtemps, même si j’avais retiré les autocollants (à vrai dire j’avais changé d’ordinateur depuis le temps).

## Troisième tentative : 2018

La troisième tentative aura été la bonne.

- J’ai commencé à me mettre les lettres dans les doigts avec Klavaro pendant une semaine
- J’ai mis le layout BÉPO par défaut sur mon PC sous GNU/Linux à la maison (avec le layout AZERTY en cas de nécessité)
- Je suis resté comme ça plusieurs semaines (avec un layout AZERTY Apple au travail)
- J’ai finalement supprimé le AZERTY du GNU/Linux

C’est à ce moment que j’ai décidé de passer le cap au travail. Je savais pertinemment que ma vitesse de frappe était ridicule, mais j’avais fait des tests avec PHPStorm et m’était persuadé (à tors ou à raison) que la vitesse serait suffisante compte tenue de l’auto-complétion.

Ça a été compliqué. On peut dire que je faisais pitiés quand je tapais du texte autre que du code (c’était presque devenus une blague avec les collègues).

Ça a été long.

Mais les résultats sont là. J’ai tenus bon et maintenant je tape en BÉPO, en aveugle.

## Retour d’expérience en tant que développeur PHP

En tant que développeur PHP, je n’ai qu’un reproche à faire à cette disposition : l’emplacement du « $ ». Il se trouve sur la première colonne de la première rangée et est donc normalement accessible via l’auriculaire gauche. Autant dire le moins précis et le moins fort des dix doigts (sauf si vous êtes gaucher peut être).

Pour ma part, j’ai résolu le problème avec PHPStorm en rajoutant un « live template » qui remplace un « s » suivis d’un espace :

![](/images/phpstorm-simple-s.png)

Pour le restant :

- Les raccourcis clavier sont vite remplacés dans la mémoire musculaire (il faut dire qu’on fait bien plus souvent des Ctrl-C/V/X qu’on ne pourrait le penser au premier abord)
- L’utilisation de Vim est très laborieuse pendant pas mal de temps (il faut vraiment faire l’effort de se rappeler quelle lettre on tape et non faire confiance à sa mémoire musculaire)

## Ce qui peut aider

Je pense qu’une chose qui a pu m’aider est que j’ai profité au même moment pour changer de clavier.

J’ai en effet découvert cette année les claviers mécaniques et j’en ai acheté un sur lequel j’ai changé les cabochons pour en mettre des vierges (cf. la photo de couverture de cet article).

Le clavier est un clavier dit « 60% » car il ne contient que 60% des touches d’un clavier « standard ». En fait, il lui manque les touches de fonctions et le pavé numérique.

Je pense que le fait de devoir faire travailler sa mémoire musculaire sans avoir la possibilité de tricher (ce qui est largement faisable en associant la touche du clavier à la lettre qu’elle remplace) a été pour moi la clef de la réussite de cette troisième tentative.

Pour ceux que ça intéresse, voici le layout exacte de ce clavier :

![](/images/rk61.png)
