---
title: "SSL : Ajouter un certificat « Let’s Encrypt » sur un site"
date: 2018-03-27
tags: ["nginx", "let's encrypt"]
---

L’objectif de cet article est de mettre en place un cetificat SSL sur une application servie par un serveur Nginx sous Ubuntu serveur.

## Installation de « Certbot »

Certbot est un outil qui automatise la création et la mise en place d’un certificat SSL « Let’s Encrypt ».

Il existe un PPA pour simplifier l’installation : https://launchpad.net/~certbot/+archive/ubuntu/certbot

```bash
$ sudo add-apt-repository ppa:certbot/certbot
$ sudo apt-get update
```

Il ne reste alors plus qu’à installer l’application :

```bash
$ sudo apt-get install certbot
```

## Configuration du SSL

Certbot possède un plugin pour Nginx nous permettant de tout automatiser :

```bash
$ sudo certbot --nginx
```

Certbot va vous demander sur quel site ajouter un certificat. Vous n’avez qu’à répondre aux quelques questions (sites, forcer HTTPS, …).

## Renouvellement automatique

Les certificats de « Let’s Encrypt » ont une durée de vie de 90 jours. Il faut donc les renouveler, et tant qu’à faire, l’automatiser.

Pour cela, le plus simple est de rajouter une tâche CRON :

```bash
30 2 * * * /usr/bin/certbot renew --noninteractive --renew-hook "/bin/systemctl reload nginx" >> /var/log/le-renew.log
```
